import React from 'react'
import { AppBar, Toolbar, styled} from '@mui/material'
import Logo from '../images/logo.jpg'

const StyledAppBar = styled(AppBar)`
background:#000;
`

const StyledLogo = styled("img")({
    width: 200,
})

const Header = () => {
    return (
        <StyledAppBar position='static' >
            <Toolbar>
                <StyledLogo src={Logo} alt="logo" />
            </Toolbar>
        </StyledAppBar>
    )
}

export default Header
