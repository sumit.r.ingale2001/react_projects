import { Card, styled, Box, Typography } from '@mui/material'
import React, { useState } from 'react'

const Image = styled('img')({
    width: "100%",
    height: 300,
    objectFit: "cover",
    objectPosition: "center"
})

const Container = styled(Box)`
position:absolute;
top:0;
color:#fff;
height:300px;
width:100%;
background:#000;
opacity:0.5;
padding:5px;
`

const StyledCard = styled(Card)`
width:300px;
margin:10px;
position:relative;
`


const Character = ({ value }) => {
    const [toggle, setToggle] = useState(false);
    const toggleHover = () => {
        setToggle(!toggle);
    }
    return (
        value.characterImageFull ?
            <StyledCard onMouseEnter={toggleHover} onMouseLeave={toggleHover} >
                <Image src={value.characterImageFull} alt="pic" />
                {
                    toggle ?
                    <Container  >
                        <Typography>Name: {value.characterName} </Typography>
                        <Typography>Real Name: {value.actorName} </Typography>
                        <Typography>House: {value.houseName} </Typography>
                        <Typography>Nickname: {value.nickname} </Typography>
                    </Container>
                    : ''
                }

            </StyledCard>
            : ' '
    )
}

export default Character
