import React, { useEffect, useState } from 'react';
import Cards from '../components/Cards/Cards';
import Inputgroup from '../components/Filters/category/Inputgroup';


const Location= () => {
    let [id, setId] = useState(1);
    let [info, setInfo] = useState([]);
    let [results, setResults] = useState([]);
    let { name, type, dimension } = info;
    let api = `https://rickandmortyapi.com/api/location/${id}`;

    useEffect(() => {
        (async function () {
            let data = await fetch(api).then(res => res.json())
            setInfo(data);

            let prom = await Promise.all(
                data.residents.map((e) => {
                    return fetch(e).then(res => res.json());
                })
            );
            setResults(prom);
        })()
    }, [api])
    return (
        <div className='container'>
            <div className="row">
                <h1 className="text-center mb-3">Location :
                    <span className="text-primary"> {name === "" ? "Unknown" : name} </span>
                </h1>
                <h5 className="text-center">Dimension{dimension === "" ? "Unknown" : dimension}</h5>
                <h6 className="text-center">Type{type === "" ? "Unknown" : type}</h6>
            </div>
            <div className="row">
                <div className="col-lg-3 col-md-12 col-sm-12">
                    <div className="text-center mb-4 fs-4 mt-4">
                    Location
                    </div>
                    <Inputgroup setId={setId} name="Location" total={126}/>
                </div>
                <div className="col-lg-9 col-md-12 col-sm-12">
                    <div className="row">
                        <Cards page="/location/" results={results} />
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Location;

