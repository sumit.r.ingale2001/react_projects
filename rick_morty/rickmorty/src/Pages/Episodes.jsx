import React, { useEffect, useState } from 'react';
import Cards from '../components/Cards/Cards';
import Inputgroup from '../components/Filters/category/Inputgroup';


const Episodes = () => {
    let [id, setId] = useState(1);
    let [info, setInfo] = useState([]);
    let [results, setResults] = useState([]);
    let { air_date, name } = info;
    let api = `https://rickandmortyapi.com/api/episode/${id}`;

    useEffect(() => {
        (async function () {
            let data = await fetch(api).then(res => res.json())
            setInfo(data);

            let prom = await Promise.all(
                data.characters.map((e) => {
                    return fetch(e).then(res => res.json());
                })
            );
            setResults(prom);
        })()
    }, [api])
    return (
        <div className='container'>
            <div className="row">
                <h1 className="text-center mb-3">Episode :
                    <span className="text-primary"> {name === "" ? "Unknown" : name} </span>
                </h1>
                <h5 className="text-center">Air Date{air_date === "" ? "Unknown" : air_date}</h5>
            </div>
            <div className="row">
                <div className="col-lg-3 col-md-12 col-sm-12">
                    <div className="text-center mb-4 fs-4 mt-4">
                    Pick Episodes
                    </div>
                    <Inputgroup setId={setId} name="Episode" total={51}/>
                </div>
                <div className="col-lg-9 col-md-12 col-sm-12">
                    <div className="row">
                        <Cards page="/episodes/" results={results} />
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Episodes
