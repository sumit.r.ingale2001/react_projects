import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import Navbar from "./components/Navbar/Navbar";
import Search from "./components/Search/Search";
import Filters from "./components/Filters/Filter";
import Cards from "./components/Cards/Cards";
import Pagination from "./components/Pagination/Pagination";
import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Episodes from "./Pages/Episodes";
import Location from "./Pages/Location";
import Carddetails from "./components/Cards/Carddetails";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
      </div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/:id" element={<Carddetails />} />

        <Route path="/episodes" element={<Episodes />} />
        <Route path="/episodes:id" element={<Carddetails />} />

        <Route path="/location" element={<Location />} />
        <Route path="/location:id" element={<Carddetails />} />

      </Routes>
    </Router>
  )
}
const Home = () => {
  // will set the page number 
  let [pageNumber, setPageNumber] = useState(1);

  // will search the character 
  let [search, setSearch] = useState("");

  // for the status whether the character is dead or alive 
  let [status, setStatus] = useState("");

  // for the gender specification 
  let [gender, setGender] = useState("");

  // for the species 
  let [species, setSpecies] = useState("");

  // data will be fetched from here 
  let [fetchedData, setFetchedData] = useState([])
  let api = `https://rickandmortyapi.com/api/character/?page=${pageNumber}&name=${search}&status=${status}&gender=${gender}&species=${species}`;
  let { info, results } = fetchedData;


  useEffect(() => {
    (async function () {
      let data = await fetch(api).then(res => res.json());
      setFetchedData(data)
    })();
  }, [api]);



  return (
    <div className="App">

      {/* search start  */}
      <div className="container my-3">
        <div className="row">
          <div className="col-lg-12">
            <h1 className="text-center">Characters</h1>
            <Search setPageNumber={setPageNumber} setSearch={setSearch} />
          </div>
        </div>
      </div>
      {/* search end */}

      {/* main section start  */}
      <div className="container">
        <div className="row" >
          <Filters setStatus={setStatus} setPageNumber={setPageNumber} setGender={setGender} setSpecies={setSpecies} />
          <div className="col-lg-9 col-sm-12 col-md-12  ">
            <div className="row">
              <Cards page="/" results={results} />
            </div>
          </div>
        </div>
      </div>
      {/* main section end */}

      {/* pagination start  */}
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-12">
            <Pagination info={info} pageNumber={pageNumber} setPageNumber={setPageNumber} />
          </div>
        </div>
      </div>
      {/* pagination end */}
    </div>
  );
}

export default App;
