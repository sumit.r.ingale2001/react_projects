import React from 'react';
import styles from './Search.module.css'

const Search = ({setSearch, setPageNumber}) => {
    return (
        <>
        <form className="d-flex flex-sm-row flex-column align-items-center justify-content-center m-2">
            <input onChange={e=>{
                setPageNumber(1);
                setSearch(e.target.value)
                }} type="text" className={`${styles.input} p-2 me-3`}  placeholder='Search the character' />
            <button onClick={e=>{e.preventDefault()}} className="btn btn-primary m-2">Search</button>
        </form>
        </>
    )
}

export default Search
