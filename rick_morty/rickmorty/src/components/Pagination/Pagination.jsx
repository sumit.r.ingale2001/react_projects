import React, { useEffect, useState } from 'react';
import ReactPaginate from "react-paginate";

const Pagination = ({ setPageNumber, pageNumber, info }) => {
    let [width, setWidth] = useState(window.innerWidth);
    let updateWidth = () => {
        setWidth(window.innerWidth)
    }
    useEffect(() => {
        window.addEventListener("resize", updateWidth)
        return () => window.removeEventListener("resize", updateWidth)
    }, []);
    return (
        <>
            <style>
                {
                    `
            @media (max-width:768px){
                .next, .prev{
                    display:none;
                }
                .pagination{
                    font-size:14px;
                }
            }
            `
                }
            </style>
            <ReactPaginate
                pageCount={info?.pages}
                className="justify-content-center pagination gap-3 my-3"
                previousLabel="Prev"
                nextLabel="Next"
                nextLinkClassName='next btn btn-primary'
                previousLinkClassName='prev btn btn-primary'
                pageClassName='page-item'
                pageLinkClassName='page-link'
                marginPagesDisplayed={width < 576 ? 1 : 2}
                pageRangeDisplayed={width < 576 ? 1 : 2}
                onPageChange={(data) => {
                    setPageNumber(data.selected + 1)
                }}
                activeClassName='active'
                forcePage={pageNumber === 1 ? 0 : pageNumber - 1}
            />
        </>
    )
}

export default Pagination;
