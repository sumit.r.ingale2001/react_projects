import { NavLink, Link } from "react-router-dom";
import '../../App.css'
const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg bg-body-tertiary mb-4">
            <div className="container">
                <Link to="/" className="fs-2 fw-bold navbar-brand">
                    Rick & Morty <span className="text-primary">App</span>
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <style>
                        {
                            `
            button[aria-expanded="false"]> .close{
                display:none;
            }            
            button[aria-expanded="true"]> .open{
                display:none;
            }            
        `
                        }
                    </style>
                    <i className="bi bi-x-lg close text-dark fw-bold"></i>
                    <i className="bi bi-list open text-dark fw-bold"></i>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul className="navbar-nav fs-5">
                        <NavLink activeClassName="active" to="/" className="nav-link">
                            Characters
                        </NavLink>
                        <NavLink to="/episodes" className="nav-link">
                            Episodes
                        </NavLink>
                        <NavLink to="/location" className="nav-link">
                            Location
                        </NavLink>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;