import React from "react";
import styles from "./Cards.module.css";
import { Link } from 'react-router-dom'


const Cards = ({ results, page }) => {
    let display;
    if (results) {
        display = results.map(card => {
            let { id, name, image, location, status } = card;
            return (
                <Link style={{ textDecoration: "none", color:"black"}} to={`${page}${id}`} className="col-lg-4 my-2 col-md-6 col-sm-12 position-relative  " key={id}>
                    <div className="card" >
                        <img src={image} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title fw-bold my-2">{name}</h5>
                            <p className="card-text">Last location</p>
                            <p className="card-text fw-semibold">{location.name}</p>
                        </div>
                        {(() => {
                            if (status === "Alive") {
                                return (
                                    <div className={`${styles.badge} bg-success badge position-absolute`}>{status}</div>
                                )
                            } else if (status === "Dead") {
                                return (
                                    <div className={`${styles.badge} bg-danger badge position-absolute`}>{status}</div>
                                )
                            } else {
                                return (
                                    <div className={`${styles.badge} bg-secondary badge position-absolute`}>{status}</div>
                                )
                            }
                        })()}

                    </div>
                </Link>

            )
        })
    } else {
        display = 'No characters found'
    }
    return (
        <>
            {display}
        </>
    )
}

export default Cards;