import React from 'react'
import FilterBtn from '../FilterBtn'

const Status = ({ setStatus, setPageNumber }) => {
    let status = ["Alive", "Dead", "Unknown"]
    return (
        <div className="accordion-item">
            <h2 className="accordion-header">
                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                    Status
                </button>
            </h2>
            <div id="collapseThree" className="accordion-collapse collapse show" data-bs-parent="#accordionExample">
                <div className="accordion-body d-flex flex-wrap gap-1">
                    {status.map((items, index) => (
                        <FilterBtn setPageNumber={setPageNumber} task={setStatus} key={index} name='status' items={items} index={index} />))}
                </div>
            </div>
        </div>
    )
}

export default Status
