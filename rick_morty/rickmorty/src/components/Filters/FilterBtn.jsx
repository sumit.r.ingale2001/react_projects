import React from 'react'

const FilterBtn = ({ name, index, items, task, setPageNumber }) => {
    return (
        <div>
            <style>
                {`
                    input[type="radio"]{
                        display:none;
                    }

                    .btncheck:checked + label{
                        background-color:#0b5ed7;
                        color:white
                    }
                `}
            </style>
            <div className="form-check">
                <input className="form-check-input btncheck"
                    onClick={() => {
                        setPageNumber(1);
                        task(items)
                    }}
                    type="radio" name={name}
                    id={`${name}-${index}`}
                />
                <label className="btn btn-outline-primary" htmlFor={`${name}-${index}`} >{items}</label>
            </div>
        </div>
    )
}

export default FilterBtn
