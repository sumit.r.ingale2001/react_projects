import Gender from "./category/Gender";
import Species from "./category/Species";
import Status from "./category/Status";
const Filters = ({ setStatus, setPageNumber, setGender, setSpecies }) => {
    let clear = () => {
        setStatus("")
        setPageNumber("")
        setGender("")
        setSpecies("")
        window.location.reload()
    }
    return (
        <div className="col-lg-3 col-md-12 col-sm-12 mt-2">
            <div className="text-center fs-4 fw-bolder mb-1">Filter</div>
            <div style={{ cursor: "pointer" }}
                onClick={clear}
                className="text-center text-decoration-underline text-primary">Clear Filters</div>
            <div className="accordion my-2" id="accordionExample">
                <Status setPageNumber={setPageNumber} setStatus={setStatus} />
                <Gender setGender={setGender} setPageNumber={setPageNumber} />
                <Species setSpecies={setSpecies} setPageNumber={setPageNumber} />
            </div>
        </div>
    )
}

export default Filters;