import React from 'react'
import { Card , styled} from '@mui/material'

const StyledCard = styled(Card)`
height:400px;
width:100%;
overflow:hidden;
box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px !important;
`

const Meme = ({meme}) => {
    return (
        <StyledCard>
            <img src={meme.data.url} alt="" style={{height:"100%", width:"100%"}} />
        </StyledCard>
    )
}

export default Meme
