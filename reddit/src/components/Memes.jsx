import { Box, Grid } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { fetchData } from '../service/api'
import Meme from './Meme'

const Memes = () => {
    const [memes, setMemes] = useState([])
    useEffect(() => {
        const getData = async () => {
            let response = await fetchData();
            setMemes(response.data.children)
        }
        getData()
    }, []);



    return (
        <Grid container style={{ padding: 20 }} >
            {
                memes.map(meme => (
                    <Grid item xs={12} sm={12} md={3} lg={3} style={{ padding: 10 }} >
                        <Meme meme={meme} />
                    </Grid>
                ))
            }
        </Grid>
    )
}

export default Memes
