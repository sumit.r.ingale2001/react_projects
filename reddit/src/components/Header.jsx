import React from 'react'
import { AppBar, Toolbar } from '@mui/material'

const URL = 'https://1000logos.net/wp-content/uploads/2017/05/Reddit-logo.jpg'
const Header = () => {
    return (
        <AppBar color='action' position='static' >
            <Toolbar>
                <img src={URL} alt="logo" style={{width:100}} />
            </Toolbar>
        </AppBar>
    )
}
export default Header;