import './App.css';
import Memes from './components/Memes';
import Home from './pages/Home';

function App() {
  return (
    <div className="App">
      <Home/>
      <Memes/>
    </div>
  );
}

export default App;
