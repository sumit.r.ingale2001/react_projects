import axios from 'axios'

const url = 'https://www.reddit.com/r/memes.json'

export const fetchData = async()=>{
    try {
        let response = await axios.get(url);
        return response.data;
    } catch (error) {
        console.log("error while calling api", error.message);
    }
}