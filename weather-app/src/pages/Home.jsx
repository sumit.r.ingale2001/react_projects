import { Box, styled } from "@mui/material"
import Bg from '../assets/bg.jpg';
import Form from "../components/Form";
import Information from "../components/Information";
import { useState } from "react";

const Image = styled(Box)({
    backgroundImage: `url(${Bg})`,
    height: "80%",
    width: "27%",
    backgroundSize: "cover",
    borderRadius: "20px 0 0 20px"
})

const Component = styled(Box)`
height:100vh;
display:flex;
align-items:center;
width:65%;
margin:0 auto
`




const Home = () => {
    // eslint-disable-next-line no-unused-vars
    const [result,setResult]= useState({})
    return (
        <Component>
            <Image></Image>
            <Box style={{width:"73%", height:"80%"}} >
                <Form setResult={setResult} />
                <Information result={result} />
            </Box>
        </Component>
    )
}

export default Home
