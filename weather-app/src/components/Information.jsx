/* eslint-disable react/prop-types */
import { Box, Typography, styled } from "@mui/material"
import { Brightness5, Brightness6, Cloud, DehazeSharp, LocationOn, Opacity, SettingsBrightness } from "@mui/icons-material"

const Row = styled(Typography)`
padding:10px;
display:flex;
align-items:center;
font-size:20px;
letter-spacing:2px;
`
const Error = styled(Typography)`
color:red;
margin:50px;
padding:20px;
`

const Information = ({ result }) => {
    return (
        result && Object.keys(result).length > 0 ?
            <Box fontStyle={{ margin: "30px 60px" }} >
                <Row><LocationOn />Location: {result.name}, {result.sys.country}</Row>
                <Row><SettingsBrightness />Temperature: {result.main.temperature}</Row>
                <Row><Opacity />Humidity: {result.main.humidity}</Row>
                <Row><Brightness5 />Sun Rise: {new Date(result.sys.sunrise * 1000).toLocaleTimeString}</Row>
                <Row><Brightness6 />Sun Set: {new Date(result.sys.sunset * 1000).toLocaleTimeString}</Row>
                <Row><DehazeSharp />{result.weather[0].main}</Row>
                <Row><Cloud />Clouds: {result.clouds.all}%</Row>
            </Box>
            :
            <Error>Please Enter the values to check weather</Error>
    )
}

export default Information
