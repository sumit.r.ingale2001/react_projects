import { Box, Button, InputBase, styled } from "@mui/material"
import { useState } from "react"
import { getWeather } from "../services/api"

const Container = styled(Box)`
background:#445A6F;
padding:10px;
`

const Input = styled(InputBase)`
margin-right:20px;
font-size:18px;
padding:5px 15px;
color:white;

`

const GetWeatherBtn = styled(Button)`
background:#e67e22;
`


// eslint-disable-next-line react/prop-types
const Form = ({ setResult }) => {
    const [data, setData] = useState({
        city: '',
        country: ''
    })

    const handleChange = (e) => {
        setData({ ...data, [e.target.name]: e.target.value })
    }

    const getWeatherInfo = async () => {
        let response = await getWeather(data.city, data.country)
        setResult(response)
    }

    return (
        <Container>
            <Input
                placeholder="city"
                onChange={(e) => handleChange(e)}
                name='city'
            />
            <Input
                placeholder="country"
                onChange={(e) => handleChange(e)}
                name='country'
            />
            <GetWeatherBtn variant="contained"
                onClick={() => getWeatherInfo()}
            >Get Weather</GetWeatherBtn>
        </Container>
    )
}

export default Form
