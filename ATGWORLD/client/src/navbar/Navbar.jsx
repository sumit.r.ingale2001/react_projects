import './Navbar.css'
import Logo from '../assets/logo.svg'
import { AiFillCaretDown, AiOutlineSearch, AiOutlineArrowLeft } from 'react-icons/ai'


const Navbar = () => {
    return (
        <div className="navbar-container d-flex align-items-center fixed-top">
            <div className='container-xxl' >
                <div className="row">
                    <div className="col-lg-12">
                        <nav className="navbar">
                            <div className="container-fluid">
                                <a className="navbar-brand">
                                    <img src={Logo} alt="logo" />
                                </a>
                                <div className="navbar-search d-flex align-items-center">
                                    <div className="search-icon d-flex justify-content-center align-items-center">
                                        <AiOutlineSearch />
                                    </div>
                                    <form className="d-flex " role="search" >
                                        <input className=" me-2 " type="search" placeholder="Search your favorite groups in ATG" aria-label="Search" />
                                    </form>
                                </div>
                                <div className='create-acc d-flex justify-content-center align-items-center' data-bs-toggle="modal" data-bs-target="#exampleModal" >
                                    Create account &nbsp;
                                    <span>Its Free!</span>&nbsp;<AiFillCaretDown />
                                </div>
                            </div>
                            <div className="second-container px-3 d-flex justify-content-between">
                                <span className='text-light fs-4' ><AiOutlineArrowLeft /></span>
                                <button data-bs-toggle="modal" data-bs-target="#exampleModal" className='btn btn-outline-light'>join group</button>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar
