export const data = [
    {
        id: 1,
        img: 'https://images.unsplash.com/photo-1426604966848-d7adac402bff?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80',
        type: "✍️ Article",
        title: "What if regular brands had regular fonts? Meet Regular Brands",
        about: "I've worked in UX for the better part of a decade. From now on, I plan to...",
        profileImg: "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        name: "Sarthak Kamra"
    },
    {
        id: 2,
        img: 'https://images.unsplash.com/photo-1544137171-9f5cf7b0fafa?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80',
        type: "🔬 Education",
        title: "Tax benifits for Investment under national scheme launched by government",
        about: "I've worked in UX for the better part of a decade. From now on, I plan to...",
        profileImg: "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        name: "Sarah West"
    },
    {
        id: 3,
        img: "https://images.unsplash.com/photo-1493238792000-8113da705763?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        type: "🗓️ Meetup",
        title: "Finance & Investment Elite Social Mixer @Lujiazui",
        extra: {
            constant: "Fri,12 Oct,2018",
            location: "Ahmedabad, India",
            color: "danger",
            text: "Visit website"
        },
        profileImg: "https://images.unsplash.com/photo-1544005313-94ddf0286df2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=388&q=80",
        name: "Ronal Jones"
    },
    {
        id: 4,
        img: "https://images.unsplash.com/photo-1571171637578-41bc2dd41cd2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
        type: "💼 Job",
        title: "Software Developer",
        extra: {
            constant: "Innovacer Analytics Private Ltd",
            location: "Noida,India",
            color: "success",
            text: "Apply on Timesjobs"
        },
        profileImg: "https://images.unsplash.com/photo-1552058544-f2b08422138a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=399&q=80",
        name: "Joseph Gray"
    }
]

