import { HiLocationMarker } from 'react-icons/hi'
import { BsFillPencilFill } from 'react-icons/bs'
import { AiOutlineExclamationCircle } from 'react-icons/ai'


const CreatePost = () => {
    return (
        <>
            {/* create post section  */}
            <div className="createpost-container hide">
                <div className='d-flex  align-items-center justify-content-between my-2' >
                    <span><HiLocationMarker />&nbsp;Noida,India</span>
                    <span><BsFillPencilFill /></span>
                </div>
                <div className="text-muted my-3">
                    <span><AiOutlineExclamationCircle />&nbsp;Your location will help us serve better and extend a personailised experience</span>
                </div>
            </div>
            <div className="pen-container d-flex justify-content-center align-items-center">
                <BsFillPencilFill />
            </div>
        </>

    )
}

export default CreatePost
