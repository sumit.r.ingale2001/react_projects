import { AiFillCaretDown } from 'react-icons/ai'

const PostLinks = () => {
    return (
        // post links 
        <div className="post-links mx-2">
            <nav className="navbar navbar-expand-lg">
                <div className="container-fluid post-links-nav">
                    <a className="navbar-brand fw-semibold" href="#" style={{ fontSize: 16 }} >All posts(32)</a>
                    <button className="navbar-toggler toggle-btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span>Filter: All&nbsp;<AiFillCaretDown /></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link text-muted" aria-current="page" href="#">Article</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Event</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Education</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Job</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default PostLinks
