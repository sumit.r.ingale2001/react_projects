import { AiFillCaretDown } from 'react-icons/ai'
import { BsPeopleFill } from 'react-icons/bs'

const PostBtn = () => {
    return (
        // right sections buttons into the topbar 
        <div className="post-btn d-flex">
            <button className="btn me-2 d-flex align-items-center gap-1 ">Write a post<AiFillCaretDown /> </button>
            <button className="btn btn-primary">
                <BsPeopleFill /> Join Group
            </button>
        </div>
    )
}

export default PostBtn
