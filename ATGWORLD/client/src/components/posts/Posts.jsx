/* eslint-disable react/prop-types */
import { BsThreeDots, BsEye, BsFillShareFill } from 'react-icons/bs'
import { AiTwotoneMedicineBox } from 'react-icons/ai'
import { HiLocationMarker } from 'react-icons/hi'
import { useState } from 'react';



const Posts = ({ id, img, type, title, extra, profileImg, name, about }) => {

    // to show the menu on clicking the three dots
    const [showDotMenu, setShowDotMenu] = useState(false);


    const handleDots = () => {
        setShowDotMenu(!showDotMenu)
    }
    return (
        <div className="card my-2" key={id} style={{ width: "100%" }}>
            <img src={img} alt="image" className="post-img" />
            <div className="p-3">
                <p className='fs-6 fw-semibold' >{type}</p>
                <div className="d-flex align-items-center justify-content-between">
                    <span className="fs-6 fw-bolder">{title}</span>
                    <span className='dots' onClick={() => handleDots()} >
                        <BsThreeDots />
                        <div className={`${showDotMenu ? "dots-menu p-2 hover" : "dots-menu p-2"}`}>
                            <ul className='list-unstyled mt-2'>
                                <li>Edit</li>
                                <li>Report</li>
                                <li>Option 3</li>
                            </ul>
                        </div>
                    </span>
                </div>
                {
                    about && <p className="text-muted about-text fw-lighter">{about}</p>
                }
                {
                    extra && (
                        <div className='d-flex align-items-center my-3  gap-5'>
                            <span className='d-flex align-items-center gap-1'> <AiTwotoneMedicineBox />{extra.constant}</span>
                            <span className='d-flex align-items-center gap-1'><HiLocationMarker /> {extra.location}</span>
                        </div>
                    )

                }
                <div className="d-flex align-items-center justify-content-between px-2 my-2">
                    <div className='d-flex align-items-center gap-2'>
                        <img src={profileImg} alt="profile img" className="profile-img" />
                        <span className='fw-bold'>{name}</span>

                    </div>
                    <div className='d-flex align-items-center gap-2'>
                        <span className='text-muted'><BsEye />&nbsp;1.4k views</span>
                        <button className='btn btn-light'>
                            <BsFillShareFill />
                        </button>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Posts
