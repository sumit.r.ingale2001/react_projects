
import PostLinks from './PostLinks'
import PostBtn from './PostBtn'
import './PostWrapper.css'
import Posts from './Posts'
import { data } from '../../data'
import CreatePost from './CreatePost'

const PostWrapper = () => {
    return (
        <div className="wrapper">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 d-flex align-items-center justify-content-between">
                        <PostLinks />
                        <PostBtn />
                    </div>
                    <hr />
                </div>

                <div className="row">
                    <div className="col-lg-8">
                        {
                            data.map((item) => {
                                return <Posts key={item.id} {...item} />
                            })
                        }
                    </div>
                    <div className="col-lg-4 px-4">
                        <CreatePost />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PostWrapper
