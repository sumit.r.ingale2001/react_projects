/* eslint-disable react/prop-types */
import './DialogContainer.css'
import { BsFacebook, BsGoogle } from 'react-icons/bs'
import Banner from '../../assets/signupBanner.svg'
import { AiOutlineClose } from "react-icons/ai"

const DialogContainer = ({ showSignin, setShowSignin }) => {
    return (
        // main container of the dialog box 
        <div className="modal fade modal-container" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header d-flex justify-content-center align-items-center">
                        {/* modal heading  */}
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Lets learn, share & inspire each other with our passion for computer engineering. Signup now 🤘
                        </h1>
                    </div>
                    <div className="modal-body">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 left">
                                    <form>
                                        {/* if someone clicked on the signin button the sign in component will render,
                                        or it will show the create account button/link that again shows the create account component
                                        */}
                                        <p>{
                                            showSignin ? "Sign in" : "Create Account"
                                        }</p>

                                        {/* inputs  */}
                                        <div className='d-flex'>
                                            <input type="text" name='firstname' placeholder='First Name' />
                                            <input type="text" name="lastname" placeholder='Last name' />
                                        </div>
                                        <input type="password" name='password' placeholder="Password" />
                                        <input type="password" name='password' placeholder="Confirm Password" />
                                        <button>{showSignin ? "Sign in" : "Create Account"}</button>
                                    </form>
                                    <div>
                                        <button className="social-btn">
                                            <BsFacebook style={{ color: "blue" }} />&nbsp;
                                            signup with facebook
                                        </button>
                                    </div>
                                    <div>
                                        <button className="social-btn">
                                            <BsGoogle style={{ color: "red" }} />&nbsp;
                                            signup with google
                                        </button>
                                    </div>
                                    {
                                        // when showSignin will be true then only show the forgot password section 
                                        showSignin && <div className='text-center fw-bold forgot-pass'>Forgot Password?</div>
                                    }
                                </div>
                                <div className="col-lg-6 right d-flex flex-column justify-content-between ">
                                    <div className="d-flex align-items-center justify-content-end text" >
                                        {/* if showSignin is true then show the first component or else the second component  */}
                                        {
                                            showSignin ? (
                                                <>
                                                    Dont have an account yet?&nbsp;
                                                    <span onClick={() => setShowSignin(!showSignin)} className="text-primary sign-btn">Create new for free!</span>
                                                </>
                                            )
                                                :
                                                (
                                                    <>
                                                        Already have an account?&nbsp;&nbsp;
                                                        <span onClick={() => setShowSignin(!showSignin)} className="text-primary sign-btn">Sign In</span>
                                                    </>
                                                )
                                        }
                                    </div>
                                    <img src={Banner} alt="banner" className='banner-img' />
                                    {/* when the showSignin is false then only show the t&c sentence  */}
                                    {
                                        !showSignin && <p className='text-center'>By signing up, you agree to our terms & conditions, Privacy policy</p>
                                    }

                                </div>
                            </div>
                        </div>
                        {/* to close the modal  */}
                        <span className='close-btn d-flex align-items-center justify-content-center' data-bs-toggle="modal" data-bs-target="#exampleModal"><AiOutlineClose /></span>

                    </div>
                </div>
            </div>
        </div >
    )
}

export default DialogContainer
