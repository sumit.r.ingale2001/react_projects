import './Banner.css'
// import bannerimg from '../../assets/banner.svg'

const Banner = () => {
    return (
        // homepage banner 
        <div className="banner">
            <div className='container-xxl banner-container'>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="px-3 text-container">
                            {/* homepage text  */}
                            <div>Computer Engineering</div>
                            <span>142,765 Computer Engineer follow this </span>
                        </div>
                    </div>
                </div>
                {/* background image of the container */}
                <div className="img-container"></div>
            </div>
        </div>
    )
}

export default Banner
