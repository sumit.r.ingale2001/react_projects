import Navbar from "./navbar/Navbar"
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import DialogContainer from "./components/Dialog/DialogContainer";
import { useState } from "react";
import Banner from "./components/banner/Banner";
import PostWrapper from "./components/posts/PostWrapper";


function App() {
  const [showText, setShowText] = useState(false)
  const [showSignin, setShowSignin] = useState(false)

  return (
    <>
      <Navbar />
      <DialogContainer setShowSignin={setShowSignin} showSignin={showSignin} setShowText={setShowText} showText={showText} />
      <Banner />
      <PostWrapper />
    </>
  )
}

export default App
