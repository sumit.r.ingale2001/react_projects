/* eslint-disable react/prop-types */
import { Box, CardContent, CardMedia, Typography } from "@mui/material"
import { Link } from "react-router-dom"
import { demoProfilePicture } from "../utils/constants"
import { CheckCircle } from "@mui/icons-material"

const ChannelCard = ({ channelDetail, marginTop }) => {
    return (
        <>
            <Box
                sx={{
                    width: "19.5rem", height: "18rem", borderRadius: "0",
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: "auto",
                    marginTop,
                }}
            >
                <Link to={`/channel/${channelDetail?.id?.channelId}`} >

                    <CardContent
                        sx={{ display: "flex", flexDirection: "column", justifyContent: "center", textAlign: "center", color: "#fff" }}
                    >
                        <CardMedia image={channelDetail?.snippet?.thumbnails?.high?.url || demoProfilePicture} alt={channelDetail?.snippet?.title}
                            sx={{ borderRadius: "50%", height: "180px", mb: 2, border: "1px solid #e3e3e3", width: "180px" }}
                        />
                        <Typography variant="h6" >
                            {channelDetail?.snippet?.title}
                            <CheckCircle sx={{ fontSize: "14px", color: "gray", ml: "5px" }} />
                        </Typography>

                        {channelDetail?.statistics?.subscriberCount && (
                            <Typography sx={{ fontSize: '15px', fontWeight: 500, color: 'gray' }}>
                                {parseInt(channelDetail?.statistics?.subscriberCount).toLocaleString('en-US')} Subscribers
                            </Typography>
                        )}
                    </CardContent>

                </Link>
            </Box>
        </>
    )
}

export default ChannelCard
