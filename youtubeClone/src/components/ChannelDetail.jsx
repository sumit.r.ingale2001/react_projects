import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import { Box } from "@mui/material"
import { Videos, ChannelCard } from './index'
import { fetchFromApi } from "../utils/fetchFromApi"




const ChannelDetail = () => {

    const [channelDetail, setChannelDetail] = useState(null);
    const [video, setVideo] = useState(null)

    const { id } = useParams()

    useEffect(() => {
        const fetchChannel = async () => {
            try {
                const data = await fetchFromApi(`channels?part=snippet&id=${id}`);
                setChannelDetail(data[0])
            } catch (error) {
                console.log(error, "error while fetching the channel details");
            }
        }

        const videoData = async () => {
            try {
                const data = await fetchFromApi(`search?channelId=${id}&part=snippet%2Cid&order=date`)

                setVideo(data)
            } catch (error) {
                console.log(error, "error while getting the video data");
            }
        }

        videoData()
        fetchChannel()
    }, [id])

    return (
        <Box minHeight="95vh" >
            <Box>
                <Box
                    sx={{
                        background: "url(https://yt3.googleusercontent.com/Fgno17YLJCAbhasp1vOZZSRD7YJUaIB5w16LZ7_r35KPVNlyiLA-dvMW48GIal9HyBU0Eo5Z3A=w1060-fcrop64=1,00005a57ffffa5a8-k-c0xffffffff-no-nd-rj)",
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        height: "300px"
                    }} />
                <ChannelCard channelDetail={channelDetail} marginTop="-93px" />
            </Box>

            <Box display="flex" p="2" >
                <Box sx={{ml:{md:"100px"}}}  >
                    <Videos videos={video} />
                </Box>
            </Box>
        </Box>
    )
}

export default ChannelDetail
