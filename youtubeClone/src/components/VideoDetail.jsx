import { useState, useEffect } from "react"
import { Link, useParams } from 'react-router-dom'
import ReactPlayer from 'react-player'
import { Typography, Box, Stack } from "@mui/material"
import { CheckCircle } from "@mui/icons-material"
import { fetchFromApi } from "../utils/fetchFromApi"

const VideoDetail = () => {

  const { id } = useParams();

  const [videoDetail, setVideoDetail] = useState(null)

  useEffect(() => {
    const fetchVideo = async () => {
      const data = await fetchFromApi(`videos?part=snippet,statistics&id=${id}`)
      setVideoDetail(data[0])
    }

    fetchVideo()
  }, [id])



  if (!videoDetail?.snippet) return "Loading";
  const { snippet: { title, channelId, channelTitle }, statistics: { viewCount, likeCount } } = videoDetail

  return (
    <Box minHeight="95vh" >
      <Stack direction={{ xs: "column", md: "row" }} >
        <Box flex={1} >
          <Box sx={{ width: "100%", position: "sticky", top: "85px" }} >
            <ReactPlayer url={`https://www.youtube.com/watch?v=${id}`} className="react-player" controls />
            <Typography sx={{ color: "#fff", fontWeight: "bold", padding: "2px" }} variant="h5" >
              {title}
            </Typography>
            <Stack direction="row" justifyContent="space-between" sx={{ color: "#fff" }} py={1} px={2} >
              <Link to={`/channel/${channelId}`} >
                <Typography variant={{ sm: "subtitle1", md: "h6" }} color="#fff" >{channelTitle}
                  <CheckCircle sx={{ fontSize: "12px", color: "gray", ml: "5px" }} />
                </Typography>
              </Link>
              <Stack direction="row" gap="20px" alignItems="center" >
                <Typography variant="body1" sx={{ opacity: "0.7" }} >{parseInt(viewCount).toLocaleString()} views</Typography>
                <Typography variant="body1" sx={{ opacity: "0.7" }} >{parseInt(likeCount).toLocaleString()} likes</Typography>
              </Stack>
            </Stack>
          </Box >
        </Box >
      </Stack >
    </Box >
  )
}

export default VideoDetail
