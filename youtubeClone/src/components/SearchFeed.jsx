import { useState, useEffect } from "react"
import { Box, Typography } from '@mui/material'
import { Videos } from "./index"
import { fetchFromApi } from "../utils/fetchFromApi"
import { useParams } from "react-router-dom"


const SearchFeed = () => {

    const [videos, setVideos] = useState([])

    const { searchTerm } = useParams()

    useEffect(() => {

        const fetchVideo = async () => {

            const data = await fetchFromApi(`search?part=snippet&q=${searchTerm}
            `)
            setVideos(data)
        }
        fetchVideo()
    }, [])

    return (
        <>
            <Box p={2} sx={{ overflowY: "auto", height: "90vh", flex: 2, }}>
                <Typography variant="h5" fontWeight="bold" mb={2} sx={{ color: "white", ml: { md: "100px", xs:"50px" } }} >
                    Search results for:&nbsp;<span style={{ color: "#f31503" }} >{searchTerm}</span>&nbsp;Videos
                </Typography>
                <Box sx={{ ml: { md: "100px" } }} >
                    <Videos videos={videos} />
                </Box>
            </Box>
        </>
    )
}

export default SearchFeed
