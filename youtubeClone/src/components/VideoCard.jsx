/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Link } from "react-router-dom"
import { Typography, Card, CardContent, CardMedia } from "@mui/material"
import { CheckCircle } from "@mui/icons-material"
import { demoThumbnailUrl, demoVideoTitle, demoVideoUrl, demoChannelUrl, demoChannelTitle } from "../utils/constants"

const VideoCard = ({ video: { id: { videoId }, snippet } }) => {
    return (
        <>
            <Card sx={{ width: "19.5rem", height: "18rem", borderRadius: "0" }} >
                <Link to={videoId ? `/video/${videoId}` : demoVideoUrl} >
                    <CardMedia image={snippet?.thumbnails?.high?.url || demoThumbnailUrl}
                        alt={snippet?.title}
                        sx={{ width: "100%", height: "60%" }}
                    />
                    <CardContent sx={{ background: "#1e1e1e", height: "40%", flexWrap: "wrap", display: "flex" }} >
                        <Link to={videoId ? `/video/${videoId}` : demoVideoUrl} >
                            <Typography variant="subtitle1" fontWeight="bold" color="#FFF">
                                {snippet?.title.slice(0, 50) || demoVideoTitle.slice(0, 50)}
                            </Typography>
                        </Link>
                        <Link to={snippet?.channelId ? `/channel/${snippet?.channelId}` : demoChannelUrl} >
                            <Typography variant="subtitle2" color="gray">
                                {snippet?.channelTitle || demoChannelTitle}
                                <CheckCircle sx={{ fontSize: "12px", color: "gray", ml: "5px" }} />
                            </Typography>
                        </Link>
                    </CardContent>
                </Link>
            </Card>
        </>
    )
}

export default VideoCard
