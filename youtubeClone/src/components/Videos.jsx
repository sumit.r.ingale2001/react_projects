/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */

import { Box, Stack } from '@mui/material'
import { ChannelCard, VideoCard } from './index'

const Videos = ({ videos }) => {
    return (
        <>
            <Stack
                direction='row'
                alignItems="center"
                gap={2} 
                flexWrap='wrap'
                sx={{justifyContent:{md:"start", sm:"center"}}}
                >
                {
                    videos.map((item, index) => (
                        <Box key={index}>
                            {item.id.videoId && <VideoCard video={item} />}
                            {item.id.channelId && <ChannelCard channelDetail={item} />}
                        </Box>
                    ))
                }
            </Stack>
        </>
    )
}

export default Videos
