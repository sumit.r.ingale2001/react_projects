/* eslint-disable no-unused-vars */
import axios from 'axios'
import { API_KEY, BASE_URL } from './keys';





const options = {
    params: {
        maxResults: "50"
    },
    headers: {
        'X-RapidAPI-Key': API_KEY,
        'X-RapidAPI-Host': 'youtube-v31.p.rapidapi.com'
    }
};


export const fetchFromApi = async (url) => {
    const { data: { items } } = await axios.get(`${BASE_URL}/${url}`, options)
    return items;
}