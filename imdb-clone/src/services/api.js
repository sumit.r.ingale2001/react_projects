import axios from 'axios'

export const categoryMovie = async (API_URL)=>{
try {
    const response = await axios.get(API_URL);
    return response.data;
} catch (error) {
    console.log("Error while calling category movie api", error.message)
    return error.message
}
}