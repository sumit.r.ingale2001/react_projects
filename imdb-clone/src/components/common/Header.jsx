import React from 'react';
import { AppBar, Toolbar, Box, styled, Typography, InputBase } from '@mui/material';
import { logoURL } from '../../constants/constant';
import { Menu, BookmarkAdd, ExpandMore } from '@mui/icons-material';
import HeaderMenu from './HeaderMenu';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';



const StyledToolbar = styled(Toolbar)`
background:#121212;
min-height: 56px !important;
padding:0 115px !important;
justify-content:space-between;
& > * {
    padding:0 16px;
}
& > div{
    display:flex;
    align-items:center;
    cursor:pointer;
    & > p{
        font-size:14px;
        font-weight:600;
    }
}
& > p{
    font-size:14px;
    font-weight:600;
}
`;

const Image = styled("img")({
    width: 80
});

const InputSearchField = styled(InputBase)`
background:#ffffff;
height:30px;
width:55%;
border-radius:5px;
`



const Header = () => {
    const [open,setOpen] = useState(null);

    const navigate = useNavigate();

    const handleClick = (e)=>{
        setOpen(e.currentTarget);
    }

    const handleClose=()=>{
    setOpen(null);
    }

    return (
        <AppBar position='static' >
            <StyledToolbar>
                <Image src={logoURL} alt="logo" onClick={()=> navigate('/')} />
                <Box onClick={handleClick}>
                    <Menu />
                    <Typography>Menu</Typography>
                </Box>
                <HeaderMenu open={open} handleClose={handleClose} />
                <InputSearchField />
                <Typography>IMDb <Box component='span' >Pro</Box>  </Typography>
                <Box>
                    <BookmarkAdd />
                    <Typography>Watchlist</Typography>
                </Box>
                <Typography>Signin</Typography>
                <Box>
                    <Typography>EN</Typography>
                    <ExpandMore />
                </Box>
            </StyledToolbar>
        </AppBar>
    )
}

export default Header
