// import Header from './Header'
import { Box, Divider, Typography, styled } from '@mui/material'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css';
import { useState, useEffect } from 'react'
import { categoryMovie } from '../../services/api'
import { useLocation } from 'react-router-dom';
import MoviesList from '../MoviesList';
import { POPULAR_API_URL, TOPRATED_API_URL, UPCOMING_API_URL, moviesType } from '../../constants/constant';

const StyledBanner = styled("img")({
    height: 450,
    width: "100%",
})

const Component = styled(Box)`
width:80%;
margin:auto;
`

const Container = styled(Box)`

background:#f5f5f5;
padding:10px;
margin: 20px 0;
` 


const responsive = {

    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};

const CategoryMovies = () => {
    const [movies, setMovies] = useState([]);

    const { search } = useLocation();

    useEffect(() => {
        const getData = async (API_URL) => {
            let response = await categoryMovie(API_URL);
            setMovies(response.results);
        }

        let API_URL;

        if (search.includes("popular")) {
            API_URL = POPULAR_API_URL;
        } else if (search.includes("upcoming")) {
            API_URL = UPCOMING_API_URL;
        } else if (search.includes("toprated")) {
            API_URL = TOPRATED_API_URL;
        }
        getData(API_URL)
    }, [search])

    return (
        <div>
            <Component>
                <Carousel responsive={responsive}
                    infinite={true}
                    swipeable={false}
                    draggable={false}
                    customTransition="all .5"
                    autoPlay={true}
                    autoPlaySpeed={3000}
                >
                    {
                        movies.map(movie => (
                            <StyledBanner src={`https://image.tmdb.org/t/p/original/${movie.backdrop_path}`} alt="banner" />
                        ))
                    }
                </Carousel>
                <Container>
                    <Typography variant='h6'>IMDb Charts</Typography>
                    <Typography variant='h4' >IMDb {moviesType[search.split('=')[1]]} movies </Typography>
                    <Typography style={{fontSize:12, margin:5}} >IMDb Top {movies.length} as rated by regular IMDb voters.</Typography>
                    <Divider/>
                    <MoviesList movies={movies} />
                </Container>
            </Component>
        </div>
    )
}

export default CategoryMovies
