import './App.css';
import Home from './pages/Home';
import CategoryMovies from './components/common/CategoryMovies';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { routePath } from './constants/route'
import Header from './components/common/Header';

function App() {
  return (
    <Router>
      <Header/>
      <Routes>
        <Route path={routePath.home} element={<Home />} />
        <Route path={routePath.categories} element={<CategoryMovies />} />
        <Route path={routePath.invalid} element={<Home />} />
      </Routes>
    </Router>
  );
}

export default App;